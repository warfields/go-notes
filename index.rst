.. Learn Rust documentation master file, created by
   sphinx-quickstart on Thu May 30 09:48:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

An Overview Of Learning Golang
==============================

Hosted on `Gitlab <https://gitlab.com/warfields/go-notes>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   go_notes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
